#[derive(Clone)]
pub struct Cell {
    pub id: String,
    pub name: String,
    pub value: String,
}
