use super::cell::Cell;

#[derive(Clone)]
pub struct Row {
    pub id: String,
    pub cells: Vec<Cell>,
}
