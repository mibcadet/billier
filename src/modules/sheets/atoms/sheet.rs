use super::row::Row;

#[derive(Clone)]
pub struct Sheet {
    pub id: String,
    pub name: String,
    pub rows: Vec<Row>,
}
