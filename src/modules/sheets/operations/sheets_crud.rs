use crate::modules::sheets::atoms::{cell::Cell, row::Row, sheet::Sheet};
use uuid::Uuid;

pub fn create_sheet(name: String) -> Sheet {
    let row: Row = Row {
        id: Uuid::new_v4().to_string(),
        cells: vec![
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Name".to_string(),
                value: "".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Unit".to_string(),
                value: "kWh".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Previous read".to_string(),
                value: "0".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Current read".to_string(),
                value: "0".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Unit price".to_string(),
                value: "0".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Other".to_string(),
                value: "0".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Amount".to_string(),
                value: "0".to_string(),
            },
            Cell {
                id: Uuid::new_v4().to_string(),
                name: "Total".to_string(),
                value: format!("0{}", "zł"),
            },
        ],
    };

    let new_sheet = Sheet {
        id: Uuid::new_v4().to_string(),
        name,
        rows: vec![row],
    };

    new_sheet
}
