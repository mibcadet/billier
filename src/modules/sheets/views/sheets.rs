use crate::modules::sheets::{atoms::sheet::Sheet, operations::sheets_crud::create_sheet};
use leptos::{
    component, create_signal, event_target_value, view, CollectView, IntoView, SignalGet,
};

fn sheet_exists(sheets: &Vec<Sheet>, name: &str) -> bool {
    sheets.iter().any(|sheet| sheet.name == name)
}

#[component]
pub fn SheetsView() -> impl IntoView {
    let (selected_sheet, set_selected_sheet) = create_signal(String::new());
    let (sheets, set_sheets) = create_signal(Vec::<Sheet>::new());

    let add_sheet = move |_| {
        let mut newName = "Sheet 1".to_string();
        let mut counter: u8 = 1;

        while sheet_exists(&sheets.get(), &newName) {
            counter += 1;
            newName = format!("Sheet {}", counter);
        }

        let mut new_sheets = sheets.get().clone();
        new_sheets.push(create_sheet(newName));
        set_sheets(new_sheets);
    };

    let remove_sheet = move |id: String| {
        let mut new_sheets = sheets.get().clone();
        new_sheets.retain(|sheet| sheet.id != id);
        set_sheets(new_sheets);
    };

    let update_sheet_name = move |id: String, new_name: String| {
        let mut new_sheets = sheets.get().clone();
        if let Some(sheet) = new_sheets.iter_mut().find(|sheet| sheet.id == id) {
            sheet.name = new_name;
        }
        set_sheets(new_sheets);
    };

    let sheets_list = move || {
        if sheets.get().is_empty() {
            set_sheets(vec![create_sheet("Sheet 1".to_string())]);
            set_selected_sheet(sheets.get().first().unwrap().id.clone());
        }

        sheets
            .get()
            .iter()
            .map(|sheet| {
                let focus_sheet_id = sheet.id.clone();
                let update_sheet_id = sheet.id.clone();
                let remove_sheet_id = sheet.id.clone();

                view! {
                    <div class=format!("font-bold border-0 border-b-[2px] rounded-t bg-gray-100 hover:bg-blue-200 cursor-pointer {}", if focus_sheet_id == selected_sheet.get().to_string() { "bg-blue-200"} else { "" })>
                        <input class="bg-transparent" value={sheet.name.clone()} on:blur=move |ev| { 
                            ev.prevent_default();
                            update_sheet_name(update_sheet_id.clone(), event_target_value(&ev))
                        }
                        on:click= move |_| {
                            set_selected_sheet(focus_sheet_id.clone());
                        }
                        prop:value=sheet.name.clone() />
                        <button class="p-2" on:click=move |_| remove_sheet(remove_sheet_id.clone().to_string())>"x"</button>
                    </div>
                }


            })
            .collect_view()
    };

    let selected_sheet_content = move || {
        sheets
            .get()
            .iter()
            .filter(|sheet| sheet.id == selected_sheet.get())
            .map(|sheet| {
                view! {
                    <div class="flex flex-col shadow-md">
                      {sheet.rows.iter().map(|row| {
                        view! {
                            <div class="flex flex-row gap-2">
                            {row.cells.iter().map(|cell| {
                                view! {
                                    <div>{cell.name.clone()}</div>
                                }
                            }).collect_view()}
                            </div>
                        }
                      }).collect_view()}
                    </div>
                }
            })
            .collect_view()
    };

    view! {
        <div class="flex flex-col">
            <div class="flex">
                {sheets_list}
                <button class="px-3 py-1 border-2 rounded-t" on:click=add_sheet>"+"</button>
            </div>
            <div>{selected_sheet_content}</div>
            <div class="flex flex-col shadow-md">
                <div class="p-2">
                    <button class="p-2 hover:shadow-md rounded">"Add record"</button>
                </div>
            </div>
        </div>
    }
}
