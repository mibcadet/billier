mod modules;
use leptos::{component, mount_to_body, view, IntoView};
use modules::sheets::views::sheets::SheetsView;

#[component]
fn App() -> impl IntoView {
    view! {
        <div class="flex flex-col gap-4 max-w-[1460px] my-0 mx-auto">
            <div class="shadow-md flex gap-2 h-[64px]"></div>
            <SheetsView />
        </div>
    }
}

fn main() {
    mount_to_body(App);
}
