/** @type {import('tailwindcss').Config} */
export default {
  content: {
    files: ["*.html", "./src/**/*.rs", "*.css", "./src/**/*.css"],
  },
  darkMode: "media",
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
